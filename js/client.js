jQuery(function () {
  window.WebSocket = window.WebSocket || window.MozWebSocket;
  
  var settings = Drupal.settings.instagram_real_time_block;
     
  var connection = new WebSocket('ws://' + settings.server + ':' + settings.port);

  connection.onopen = function () {
    // connection is opened and ready to use
  };

  connection.onerror = function (error) {
    // an error occurred when sending/receiving data
  };
  
  // Shuffles an array.
  function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }  

  connection.onmessage = function (message) {
    var json = JSON.parse(message.data);
    var i = 0;
    var html = '';
    var urls = [];
    var images = jQuery('.instagram-rtb img');

    for (i = 0; i < images.length; i++) {
      urls[i] = {
        url:jQuery(images[i]).attr('src'),
        id:i
      };
    }

    // Push the new content onto array.
    urls.push({url:json.data.images.standard_resolution.url,id:images.length});
    
    // Randomize the items in the array.
    urls = shuffle(urls);

    for (i = 0; i < urls.length; i++) {
      var block_class = (settings.block_class ? settings.block_class : '');
      html += '<div class="item ' + block_class + '" data-id="item-' + urls[i].id + '">';
      html += '<img src="' + urls[i].url + '" />';
      html += '</div>';
    }

    jQuery('.instagram-rtb').quicksand(jQuery(html).find('div').prevObject, {
      adjustHeight: 'dynamic',
      useScaling: true
    });
  };
});
