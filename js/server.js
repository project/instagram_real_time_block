var WebSocketServer = require('websocket').server;
var http = require('http');
var port = 8080;
var server = http.createServer(function(request, response) {

});

function parseArguments() {
  var args = process.argv.slice(2);
  var i = args.indexOf('-p');
  if(i >= 0 && args.length >= 2) {
  	port = args[i+1];
  }
}

parseArguments();

server.listen(port, function() { });

// create the server
wsServer = new WebSocketServer({
  httpServer: server
});
var clients = [];

// WebSocket server
wsServer.on('request', function(request) {
  var connection = request.accept(null, request.origin);
  var index = clients.push(connection) - 1;

  connection.on('connect', function() {
    console.log('connecting client.');
  });

  connection.on('message', function(message) {
    if (message.type === 'utf8') {
      var json = JSON.parse(message.utf8Data);

      for (var i = 0; i < clients.length; i++) {
        clients[i].send(message.utf8Data);
      }
    }
  });

  connection.on('error', function(error) {
    console.log('error' + error);
  });

  connection.on('close', function(connection) {
    // close user connection
    console.log('closing');
  });
});
