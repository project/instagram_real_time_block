Instagram Real Time Block
=========================

Display real-time media updates in
a block. 
The module uses a websocket to push
updates to the clients.
Notice that you need to run a websocket 
server for this to work.

Installation
============

- Download the module and place it somewhere under the 
`sites/all/modules` directory.
- Install the needed packages, preferably using composer 
by standing in the modules directory and execute `composer install`.
- Install the necessary node modules by executing
`npm install` from the js folder.
- Enable the module and go to the configuration page located at 
`admin/config/media/instagram_real_time_block`.
- Add your client_id and client_secret or use the defaults.
- You also need to configure the redirect uri, this value
could for instance be set to the root of your webpage i.e
http://example.com/.
- Add the ip/adress and port for your websocket server.
- Enable the `Instagram Real-Time Block` under 
`admin/structures/blocks`.

Running
=======

- Start the websocket server: `nodejs js/server.js`.
