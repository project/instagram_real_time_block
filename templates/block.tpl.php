<?php

/**
 * @file
 * Instagram real time block.
 */
?>
<div class="instagram-rtb">
  <div class="login-wrapper">
    <a class="login-button" href="<?php print $login_url; ?>"><?php print t('Login'); ?></a>
  </div>
  <?php if (count($media)): ?>
    <?php foreach ($media as $index => $url): ?>
    <div class="item<?php print ($class ? ' ' . $class : ''); ?>">
      <img src="<?php print $url; ?>" />
    </div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
